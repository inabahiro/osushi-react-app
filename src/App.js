import React from 'react';
import { Redirect, BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './components/header'
import{ useCookies } from "react-cookie"
import top from './components/main/top';
import login from './components/user/login';
import Signup from './components/user/signup';
import editPass from './components/user/editPass';
import logout from './components/user/logout';
import EditUser from './components/user/editUser';
import {resultPage} from './components/result/result'
import question from './components/question/question';
import {AdminPage} from './components/user/admin';






function App() {
  const [cookies] = useCookies(['user']);
  return (
    <BrowserRouter>
    <Header/>
      <Switch>
      <Route exact path='/login' component={login} />
      {cookies.isSignedIn ?
        <Switch>
        <Route exact path='/' component={top} />
        <Route exact path='/signup' component={Signup} />
        <Route exact path='/edit' component={EditUser} />
        <Route exact path='/edit_pass' component={editPass} />
        <Route exact path='/logout' component={logout} />
        <Route exact path='/result' component={resultPage} />
        <Route exact path='/question' component={question} />
        <Route exact path='/admin' component={AdminPage} />
        </Switch>
      :<Redirect to={'/login'} />}
      </Switch>
    </BrowserRouter>
  );
}
export default App;
