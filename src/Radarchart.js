
import moment from 'moment';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
    Radar, RadarChart, PolarGrid, Legend,
    PolarAngleAxis, PolarRadiusAxis,
} from 'recharts';



// Chartコンポーネントの定義
export default class Chart extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            userResult: [],
        }
    }
    componentDidMount() {
        const id = this.props.id
        fetch("http://localhost:8080/osushi/sendResult/" + id, {
            method: "GET"
        })
            .then((response) => {
                response.json().
                    then(json => {
                        this.setState({
                            userResult: json
                        })
                    })
            })

    }

    render() {
        //日付を年月日の形に変更
        const resultLists = this.state.userResult;
        const date = resultLists.map((time) =>
            time.createdDate
        );
        const m = moment(date[0])
        const m1 = moment(date[1])
        const formatDate = m.format('YYYY年MM月DD日')
        const formatDate1 = m1.format('YYYY年MM月DD日')

        //最新の結果のフラグの結果を抽出
        const createtypes = resultLists.map((flg) =>
            flg.create_flg);


        const volunteertypes = resultLists.map((flg) =>
            flg.volunteer_flg);


        const drivetypes = resultLists.map((flg) =>
            flg.drive_flg);


        const analyzetypes = resultLists.map((flg) =>
            flg.analyze_flg);




        const result = resultLists.map((typeResult) => [
            { subject: 'ドライブ', A: typeResult.drive_type, fullMark: 10 },
            { subject: 'アナライズ', A: typeResult.analyze_type, fullMark: 10 },
            { subject: 'クリエイト', A: typeResult.create_type, fullMark: 10 },
            { subject: 'ボランティア', A: typeResult.volunteer_type, fullMark: 10 },

        ]);



        if (resultLists.length !== 0) {
            return (
                <div className="radarChart">

                    <RadarChart  // レーダーチャート全体の設定を記述
                        cx={250}  // 要素の左端とチャートの中心点との距離(0にするとチャートの左半分が隠れる)
                        cy={250}  // 要素の上部とチャートの中心点との距離(0にするとチャートの上半分が隠れる)
                        outerRadius={150}  // レーダーチャート全体の大きさ
                        width={500}  // レーダーチャートが記載される幅(この幅よりチャートが大きい場合、はみ出た箇所は表示されない)
                        height={500}   // レーダーチャートが記載される高さ
                        data={result[0]}  // 表示対象のデータ
                        className="radarChart"
                    >
                        {/* レーダーチャートの蜘蛛の巣のような線 */}
                        <PolarGrid />
                        {/* 項目を決めるデータのキー(サンプルでいう数学や歴史) */}
                        <PolarAngleAxis dataKey="subject" />

                        {/* 目安となる数値が表示される線を指定  */}
                        <PolarRadiusAxis
                            angle={30}  // 中心点から水平を0°とした時の角度 垂直にしたいなら90を指定
                            domain={[0, 20]}  // リストの１番目の要素が最小値、2番目の要素が最大値
                        />

                        {/* レーダーを表示 */}

                        <Radar
                            name={formatDate}  // そのチャートが誰のデータか指定(チャート下にここで指定した値が表示される)
                            dataKey="A"   // 表示する値と対応するdata内のキー
                            stroke="#8884d8"  // レーダーの外枠の色
                            fill="#8884d8"  // レーダー内の色
                            fillOpacity={0.6}  // レーダー内の色の濃さ(1にすると濃さMAX)
                        />
                        {/* ２個目のレーダー */}
                        {resultLists[1] && <Radar name={formatDate1} dataKey="A" stroke="#82ca9d" fill="#82ca9d" fillOpacity={0.6} data={result[1]} />}
                        {/* グラフの下のAさんBさんの表記 */}
                        <Legend />
                    </RadarChart>
                    {/* タイプ結果表示 */}
                    <a>{this.props.name}さんは</a>
                    {createtypes[0] && <a className="typeFont">クリエイト</a>}
                    {volunteertypes[0] && <a className="typeFont">ボランティア</a>}
                    {drivetypes[0] && <a className="typeFont">ドライブ</a>}
                    {analyzetypes[0] && <a className="typeFont">アナライズ</a>}
                    <a>の傾向が強いです</a>
                </div>
            );
        } else {
            return (
                <p className="resultNot">🍣{this.props.name}さんは診断未実施です🍣</p>
            );
        }
    }
}
Chart.propTypes = {
    dispatch: PropTypes.func,
    id: PropTypes.any,
    name: PropTypes.any,
    types: PropTypes.array
}
