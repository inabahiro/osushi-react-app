
export const LOGIN = "LOGIN";
export const LOGIN_STATE = "LOGIN_STATE";
export const DEPARTMENT = "DEPARTMENT";
export const USERS = "USERS";

export function login(loginUser) {
  return {
    type: LOGIN,
    loginUser: loginUser,
  }
}

export function loginState(bool) {
  return {
    type: LOGIN_STATE,
    isLogined: bool,
  }

}

export function departmentId(departmentId) {
  return {
    type: DEPARTMENT,
    departmentId: departmentId,
  }
}

export function getUsers(users) {
  return {
    type: USERS,
    users: users,
  }
}
