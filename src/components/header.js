import React from 'react';
import { Link } from 'react-router-dom'
import '../css/header.css';
import { withCookies } from 'react-cookie';
import * as actions from '../action/actions'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Logout from '../components/user/logout';


class Header extends React.Component {
  constructor(props) {
    super(props)
    const { dispatch } = props;
    const { cookies } = this.props;
    this.action = bindActionCreators(actions, dispatch)
    this.state = {
      loginUser: cookies.get('user')
    }
  }

  render() {

    const loginFlg = this.props.allCookies.isSignedIn
    const adminFlg = this.state.loginUser.adminFlg

    var loginLink;
    var questionLink;
    var editPassLink;
    var homeLink;
    if (loginFlg) {
      questionLink = <li><Link to="/question" className="link">診断</Link></li>;
      editPassLink = <li><Link to="/edit_pass" className="link">パスワード変更</Link></li>;
      loginLink = <Logout />;
      homeLink = <li className="home"><Link to="/">🍣おすし</Link></li>
    }

    var adminLink;
    var signupLink;
    if (adminFlg && loginFlg) {
      adminLink = <li><Link to="/admin" className="link">管理画面</Link></li>;
      signupLink = <li><Link to="/signup" className="link">新規登録</Link></li>;
    }

    return (
      <nav className='header'>
        <ul className='header-link'>
          {homeLink}
          {adminLink}
          {signupLink}
          {questionLink}
          {editPassLink}
          {loginLink}
        </ul>
      </nav>
    )
  }
}




function mapStateToProps(state) {
    return state
}
Header.propTypes = {
  cookies: PropTypes.any,
  allCookies: PropTypes.any,
  dispatch: PropTypes.func,
  history: PropTypes.object,
  isLogined: PropTypes.any,
}

export default withCookies(withRouter(connect(mapStateToProps)(Header)));
