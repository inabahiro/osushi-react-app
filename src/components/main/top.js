import React, {useState, useEffect } from 'react'
import { withRouter } from "react-router";
import { withCookies, useCookies } from 'react-cookie';
import  TopRadarChart  from '../result/topRadarChart.js';
import { FeatureType } from '../result/featureType.js';
import { Link } from 'react-router-dom'


function Top(){

  //クッキーからログイン情報取得
  const [cookies] = useCookies(['user']);
  const loginUser = cookies.user

  //取得したjsonデータを持っておくためのState
  const [result, setResult] = useState([])

  //jsonデータの取得
  useEffect(() => {
    fetch("http://localhost:8080/osushi/sendResult/" + loginUser.id,{
      method: "GET"
    })
    .then((response) => response.json())
    .then(data => {
      setResult(data);
    })
  },[]
  )

  //条件分岐
  //実施後の管理者
  if(loginUser.adminFlg === true && result.length !== 0){
    return (
      <div>
      <a className="loginName">お疲れ様です! {cookies.user.name}さん</a>
      <FeatureType />
      <TopRadarChart />
      </div>
    );
    //実施前の管理者
  }else if(loginUser.adminFlg === true && result.length === 0){
    return (
      <div>
      <a className="loginName">お疲れ様です! {cookies.user.name}さん</a>
      <FeatureType />
      <p className="radarChart"> 下記リンクから診断を始めましょう！　</p>
      <p className="radarChart"><Link to="/question" className="questionLink">診断</Link></p>
      </div>
    );
    //実施後の一般ユーザー
  } else if(loginUser.adminFlg === false && result.length !== 0){
      return (
        <div>
        <a className="loginName">お疲れ様です! {cookies.user.name}さん</a>
        <FeatureType />
        <TopRadarChart />
        </div>
      );
      //実施前の一般ユーザー
    }else if(loginUser.adminFlg === false && result.length === 0){
      return (
        <div>
        <a className="loginName">お疲れ様です! {cookies.user.name}さん</a>
        <FeatureType />
        <p className="radarChart"> 下記リンクから診断を始めましょう！　</p>
        <p className="radarChart"><Link to="/question" className="questionLink">診断</Link></p>
        </div>
      );
  }

}

export default withCookies(withRouter(Top));
