import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import * as actions from '../../action/actions'
import 'whatwg-fetch';
import { withCookies } from 'react-cookie';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';
import '../../css/question.css';



class Question extends Component {

  constructor(props) {
    super(props)
    const { dispatch } = props;
    const { cookies } = this.props;
    this.action = bindActionCreators(actions, dispatch)
    this.state = {
      //クッキーからログインユーザーの情報を取得
      loginUser: cookies.get('user'),
      //質問リストのリストを作成
      questionList: [],
      //1番目の回答
      firstAnswers:{
        q1_a1:'',
        q2_a1:'',
        q3_a1:'',
        q4_a1:'',
        q5_a1:'',
        q6_a1:'',
        q7_a1:'',
        q8_a1:'',
        q9_a1:'',
        q10_a1:''
      },

      //2番目の回答
      secondAnswers:{
        q1_a2:'',
        q2_a2:'',
        q3_a2:'',
        q4_a2:'',
        q5_a2:'',
        q6_a2:'',
        q7_a2:'',
        q8_a2:'',
        q9_a2:'',
        q10_a2:''
      },
      //モチベーションタイプのスコア
      TypeScore:{
        userId: 0,
        driveScore: 0,
        analyzeScore: 0,
        createScore: 0,
        volunteerScore: 0
      },
      //バリデーションメッセージ
      duplicationError:"",
      shortageError:""
    }
  }//constructor閉じ

  //質問リストを取得してステートにセット
  componentDidMount() {
    fetch("http://localhost:8080/osushi/sendQuestion", {
      method: "GET"
    })
    .then((response) => {
      response.json().
      then(json => {
        //console.log(json)
        this.setState({ questionList: json })
      })
    })
  }


  render() {

    //1番目の回答のラジオボタンを押したときの処理
    const firstSelectHandleChange = (e) => {
      this.setState({...this.state,firstAnswers:{
        ...this.state.firstAnswers,[e.target.name]:e.target.value
      }})
    }

    //2番目の回答のラジオボタンを押したときの処理
    const secondSelectHandleChange = (e) => {
      this.setState({...this.state,secondAnswers:{
        ...this.state.secondAnswers,[e.target.name]:e.target.value
      }})
    }

    //送信ボタンを押したときの処理
    const sendButtonClicked = () => {

      const firstAnswers = Object.values(this.state.firstAnswers);
      const secondAnswers = Object.values(this.state.secondAnswers);


      let count = 0
      this.setState({shortageError:""})
      this.setState({duplicationError:""})

      //未回答のバリデーション
      for(let i = 0;  i< 10; i++){
        if(firstAnswers[i]===('') || secondAnswers[i]===('') ){
          this.setState({shortageError:'未回答の質問があります'})
          count++;
        }
      }

      //重複回答のバリデーション
      for(var j = 0; j < 10; j++){
        if(firstAnswers[j] === secondAnswers[j] && firstAnswers[j] != ("")){
          this.setState({duplicationError:'1番目の回答と2番目の回答が重複している質問があります'})
          count++;
        }
      }

      if(count > 0){
        return
      }

      let driveScore = 0;
      let analyzeScore = 0;
      let createScore = 0;
      let volunteerScore = 0;

      //1番目の回答の点数計算
      //switchは使えないみたい...
      for(var k = 0; k < 10; k++){
        if(firstAnswers[k]===('drive')){
          driveScore += 2;
        }
        if(firstAnswers[k]===('analyze')){
          analyzeScore += 2;
        }
        if(firstAnswers[k]===('create')){
          createScore += 2;
        }
        if(firstAnswers[k]===('volunteer')){
          volunteerScore += 2;
        }
      }

      //2番目の回答の点数計算j
      for(var l = 0;  l< 10; l++){
        if(secondAnswers[l]===('drive')){
          driveScore += 1;
        }
        if(secondAnswers[l]===('analyze')){
          analyzeScore += 1;
        }
        if(secondAnswers[l]===('create')){
          createScore += 1;
        }
        if(secondAnswers[l]===('volunteer')){
          volunteerScore += 1;
        }
      }

      //確認
      console.log(driveScore)
      console.log(analyzeScore);
      console.log(createScore);
      console.log(volunteerScore);

      //タイプのスコアとログインユーザーIDをresultにセット
      const result = Object.assign({}, this.state.TypeScore)
      result.driveScore = driveScore
      result.analyzeScore = analyzeScore
      result.createScore = createScore
      result.volunteerScore = volunteerScore
      result.userId=this.state.loginUser.id
      //診断結果をpostSend関数へ
      postSend(result)
    }

    //診断結果を送信
    const postSend = (result) => {

      var confirmation = confirm('送信しますか');
      if(!confirmation) {
        return
      }

      fetch("http://localhost:8080/osushi/getResult", {
        method: "POST",
        body: JSON.stringify(result)
      })
        .then((response) => {
          if (response.status === 200) {
            this.props.history.push("/")
          } else {
            alert("送信に失敗しました。")
          }
        })
        .catch(error => console.error(error));
    }

    //質問フォーム全体
    const questions = this.state.questionList.map((q) => (
      <form key={q.id}>
        {/*質問文*/}
        <p className="question-text">Q{q.id}.{q.text}？</p>
        <p>１番目に当てはまるものを選択してください</p>

        {/*Driveタイプのラジオボタンと選択肢*/}
        <input
          type="radio"
          name={'q' + q.id + '_a1'}
          value="drive"
          onChange={firstSelectHandleChange}
        />
        {q.driveSelect}
        <br/>

        {/*Analyzeタイプのラジオボタンと選択肢*/}
        <input
          type="radio"
          name={'q' + q.id + '_a1'}
          value="analyze"
          onChange={firstSelectHandleChange}
        />
        {q.analyzeSelect}
        <br/>

        {/*Createタイプのラジオボタンと選択肢*/}
        <input
          type="radio"
          name={'q' + q.id + '_a1'}
          value="create"
          onChange={firstSelectHandleChange}
        />
        {q.createSelect}
        <br/>

        {/*Volunteerタイプのラジオボタンと選択肢*/}
        <input
          type="radio"
          name={'q' + q.id + '_a1'}
          value="volunteer"
          onChange={firstSelectHandleChange}
        />
        {q.volunteerSelect}
        <br/>
        <br/>


        <p>２番目に当てはまるものを選択してください</p>
        {/*Driveタイプのラジオボタンと選択肢*/}
        <input
          type="radio"
          name={'q' + q.id + '_a2'}
          value="drive"
          onChange={secondSelectHandleChange}
        />
        {q.driveSelect}
        <br/>

        {/*Analyzeタイプのラジオボタンと選択肢*/}
        <input
          type="radio"
          name={'q' + q.id + '_a2'}
          value="analyze"
          onChange={secondSelectHandleChange}
        />
        {q.analyzeSelect}
        <br/>

        {/*Createタイプのラジオボタンと選択肢*/}
        <input
          type="radio"
          name={'q' + q.id + '_a2'}
          value="create"
          onChange={secondSelectHandleChange}
        />
        {q.createSelect}
        <br/>

        {/*Volunteerタイプのラジオボタンと選択肢*/}
        <input
          type="radio"
          name={'q' + q.id + '_a2'}
          value="volunteer"
          onChange={secondSelectHandleChange}
        />
        {q.volunteerSelect}
        <br/>

      </form>
    )) //questions閉じ


    return (
      <div className="main">
        <div className="question">
        {questions}
        </div>
        <div className="question-error">
          <p>{this.state.duplicationError}</p>
          <p>{this.state.shortageError}</p>
        </div>
        <Button className="question-button" onClick={sendButtonClicked}>送信する</Button>
      </div>
    )//return閉じ
  }//render閉じ
}//class閉じ


Question.propTypes = {
  cookies: PropTypes.any,
  dispatch: PropTypes.func,
  history: PropTypes.object
}


function mapStateToProps(state) {
    return state
}

export default withCookies(withRouter(connect(mapStateToProps)(Question)));
