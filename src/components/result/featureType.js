import React, {Component} from 'react'
import Modal from 'react-modal';
import '../../css/featureType.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button } from 'react-bootstrap';

export class FeatureType extends Component{

  constructor () {
    super();
    this.state = {
      driveModel: false,
      volunteerModel: false,
      analyzeModel: false,
      createModel: false,
    };
    // ドライブ
    this.driveOpenModal = this.driveOpenModal.bind(this);
    this.driveCloseModal = this.driveCloseModal.bind(this);
    // ボランティア
    this.volunteerOpenModal = this.volunteerOpenModal.bind(this);
    this.volunteerCloseModal = this.volunteerCloseModal.bind(this);
    // アナライズ
    this.analyzeOpenModal = this.analyzeOpenModal.bind(this);
    this.analyzeCloseModal = this.analyzeCloseModal.bind(this);
    // クリエイト
    this.createOpenModal = this.createOpenModal.bind(this);
    this.createCloseModal = this.createCloseModal.bind(this);
  }
  // ドライブ
  driveOpenModal () {
    this.setState({ driveModel: true });
  }
  driveCloseModal () {
    this.setState({ driveModel: false });
  }
  // ボランティア
  volunteerOpenModal () {
    this.setState({ volunteerModel: true });
  }
  volunteerCloseModal () {
    this.setState({ volunteerModel: false });
  }
  // アナライズ
  analyzeOpenModal () {
    this.setState({ analyzeModel: true });
  }
  analyzeCloseModal () {
    this.setState({ analyzeModel: false });
  }
  // クリエイト
  createOpenModal () {
    this.setState({ createModel: true });
  }
  createCloseModal () {
    this.setState({ createModel: false });
  }


  render(){
    return (
      <div className="modalButton">
        <div>
          <Button variant="outline-danger" onClick={this.driveOpenModal}>ドライブ</Button>
          <Modal className="content" isOpen={this.state.driveModel} onRequestClose={this.driveCloseModal}>
            <div>
              <Button className="modalClose" variant="outline-danger" onClick={this.driveCloseModal}>Close</Button>
              <h5 className="modalTitle">ドライブタイプ</h5>
              <table>
                <tr>
                  <th>欲求</th>
                  <th>KEY</th>
                  <th>初対面</th>
                  <th>Happy Word</th>
                  <th>趣向</th>
                  <th>ストレス</th>
                  <th>強み</th>
                  <th>課題</th>
                </tr>
                <tr>
                  <td>達成支配欲求</td>
                  <td>
                    <p>勝ち負け</p>
                    <p>敵味方</p>
                    <p>損得</p>
                  </td>
                  <td>なんぼのもんだ</td>
                  <td>すごい</td>
                  <td>
                    <p>自力本願で強くありたい</p>
                    <p>周囲に影響を与えたい</p>
                    <p>意志薄弱状態や他者依存を避けたい</p>
                    <p>プロセスより結果</p>
                    <p>目的や目標達成を追求</p>
                    <p>他者との競争を勝ち抜く、抜きに出る</p>
                  </td>
                  <td>
                    <p>結果の数値化があいまい</p>
                    <p>競争の原理があいまい</p>
                  </td>
                  <td>
                    <p>牽引力</p>
                    <p>主張力</p>
                    <p>主体性</p>
                    <p>達成意欲</p>
                  </td>
                  <td>
                    <p>受容力</p>
                    <p>傾聴力</p>
                    <p>中立性</p>
                    <p>自制心</p>
                  </td>
                </tr>
              </table>
            </div>
          </Modal>
        </div>

        <div>
          <Button variant="outline-success" onClick={this.volunteerOpenModal}>ボランティア</Button>
          <Modal className="content" isOpen={this.state.volunteerModel} onRequestClose={this.volunteerCloseModal}>
            <div>
              <Button className="modalClose" variant="outline-success" onClick={this.volunteerCloseModal}>Close</Button>
              <h5 className="modalTitle">ボランティアタイプ</h5>
              <table>
                <tr>
                  <th>欲求</th>
                  <th>KEY</th>
                  <th>初対面</th>
                  <th>Happy Word</th>
                  <th>趣向</th>
                  <th>ストレス</th>
                  <th>強み</th>
                  <th>課題</th>
                </tr>
                <tr aligin="center">
                  <td>貢献調停欲求</td>
                  <td>
                    <p>善悪</p>
                    <p>正邪</p>
                    <p>愛憎</p>
                  </td>
                  <td>いい人なのかどうか</td>
                  <td>ありがとう</td>
                  <td>
                    <p>人から愛されたい</p>
                    <p>平和を保ち葛藤を避けたい</p>
                    <p>中立的な立場でいたい</p>
                    <p>他者との戦いより協調を大切にしたい</p>
                    <p>周囲の期待に応えたい</p>
                    <p>人からの支持、感謝されたい</p>
                    <p>結果よりプロセス</p>
                  </td>
                  <td>激しい競争</td>
                  <td>
                    <p>調停力</p>
                    <p>受容力</p>
                    <p>中立性</p>
                    <p>自制心</p>
                  </td>
                  <td>
                    <p>牽引力</p>
                    <p>主張力</p>
                    <p>主体性</p>
                    <p>達成意欲</p>
                  </td>
                </tr>
              </table>
            </div>
          </Modal>
        </div>

        <div>
          <Button variant="outline-info" onClick={this.analyzeOpenModal}>アナライズ</Button>
          <Modal className="content" isOpen={this.state.analyzeModel} onRequestClose={this.analyzeCloseModal}>
            <div>
              <Button className="modalClose" variant="outline-info" onClick={this.analyzeCloseModal}>Close</Button>
              <h5 className="modalTitle">アナライズタイプ</h5>
              <table>
                <tr>
                  <th>欲求</th>
                  <th>KEY</th>
                  <th>初対面</th>
                  <th>Happy Word</th>
                  <th>趣向</th>
                  <th>ストレス</th>
                  <th>強み</th>
                  <th>課題</th>
                </tr>
                <tr aligin="center">
          	      <td>論理探究欲求</td>
          	      <td>
          		      <p>真偽</p>
          		      <p>因果</p>
          		      <p>優劣</p>
          	      </td>
          	      <td>言っていることは正しいのか</td>
          	      <td>
          		      <p>確かに</p>
          		      <p>正しい</p>
          	      </td>
          	      <td>
          		      <p>様々な知識を吸収したい</p>
          		      <p>複雑な物事を究明して自信を持ちたい</p>
          		      <p>勢いだけで走ることを避けたい</p>
          		      <p>無計画な状態を避けたい</p>
          		      <p>合理的な答えを導き出したい</p>
          		      <p>客観的な視点を軽視しない</p>
          	      </td>
          	      <td>
          		      <p>大雑把な物事</p>
          		      <p>合理的な意味付けのない仕事</p>
          	      </td>
          	      <td>
          		      <p>分析力</p>
          		      <p>計画力</p>
          		      <p>探求心</p>
          		      <p>起立性</p>
          	      </td>
          	      <td>
          		      <p>発想力</p>
          		      <p>直観力</p>
          		      <p>柔軟性</p>
          		      <p>変化志向</p>
          	      </td>
                </tr>
              </table>
            </div>
          </Modal>
        </div>

        <div>
          <Button variant="outline-warning" onClick={this.createOpenModal}>クリエイト</Button>
          <Modal className="content" isOpen={this.state.createModel} onRequestClose={this.createCloseModal}>
            <div>
              <Button className="modalClose" variant="outline-warning" onClick={this.createCloseModal}>Close</Button>
              <h5 className="modalTitle">クリエイトタイプ</h5>
              <table>
                <tr>
                  <th>欲求</th>
                  <th>KEY</th>
                  <th>初対面</th>
                  <th>Happy Word</th>
                  <th>趣向</th>
                  <th>ストレス</th>
                  <th>強み</th>
                  <th>課題</th>
                </tr>
                <tr aligin="center">
          	      <td>感性発散欲求</td>
          	      <td>
          		      <p>好き嫌い</p>
          		      <p>苦楽</p>
          		      <p>美醜</p>
          	      </td>
          	      <td>おもしろい人なのか</td>
          	      <td>
          		      <p>新しい</p>
          		      <p>おもしろい</p>
          	      </td>
          	      <td>
          		      <p>新しいものを生み出したい</p>
          		      <p>楽しいことを計画したい</p>
          		      <p>自分の個性を理解されたい</p>
          		      <p>平凡であることを避けたい</p>
          		      <p>同じことの繰り返しを避けたい</p>
          		      <p>変化感や変革を実現したい</p>
          		      <p>自由な発想、型にはまらないことを望む</p>
          	      </td>
          	      <td>
          		      <p>マニュアル化の域を出ない</p>
          		      <p>アイディア発揮の余地がない</p>
          	      </td>
          	      <td>
          		      <p>発想力</p>
          		      <p>直観力</p>
          		      <p>柔軟性</p>
          		      <p>変化志向</p>
          	      </td>
          	      <td>
          		      <p>持続力</p>
          		      <p>計画力</p>
          		      <p>探究心</p>
          		      <p>規律性</p>
          	      </td>
                </tr>
              </table>
            </div>
          </Modal>
        </div>
      </div>
    );
  }
}
