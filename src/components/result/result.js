import React, { Component } from 'react'
import { withRouter } from "react-router";
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { withCookies } from 'react-cookie';
import { bindActionCreators } from 'redux'
import Chart from '../../RadarChart'
import * as actions from '../../action/actions.js';
import '../../css/result.css';



class result extends Component {

  constructor(props) {
    super(props)
    const { cookies } = this.props;
    const { dispatch } = props;
    this.action = bindActionCreators(actions, dispatch);
    this.state = {
      loginUser: cookies.get('user'),
    }
  }


  render() {
    if (!this.state.loginUser.adminFlg) {
      alert("管理権限を持つアカウントでアクセスしてください\nトップに戻ります")
      window.location.href = "../"
    }
    if (this.props.location.state == undefined) {
      return (
        <h3>🙅管理画面からユーザーを選択してください</h3>
      )
    } else {
      return (
        <div>
          <h3 className="modalTitle">{this.props.location.state.user.name}さんの診断結果</h3>
          <Chart
            id={this.props.location.state.user.id}
            name={this.props.location.state.user.name}
          />
        </div>
      )
    }
  }
}
result.propTypes = {
  dispatch: PropTypes.func,
  location: PropTypes.any,
  cookies: PropTypes.any,
}
function mapStateToProps(state) {
  return state
}

export const resultPage = withCookies(withRouter(connect(mapStateToProps)(result)))
