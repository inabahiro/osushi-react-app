import React, { useState, useEffect } from 'react'
import { withCookies, useCookies } from 'react-cookie';
import '../../css/featureType.css';

import {
  Radar, RadarChart, PolarGrid, Legend,
  PolarAngleAxis, PolarRadiusAxis,
} from 'recharts';
import moment from 'moment';


// コンポーネントの定義
function TopRadarChart() {

  //クッキーからログイン情報取得
  const [cookies] = useCookies(['user']);
  const loginUser = cookies.user

  //取得したjsonデータを持っておくためのState
  const [results, setResults] = useState([])

  //jsonデータの取得
  useEffect(() => {
    fetch("http://localhost:8080/osushi/sendResult/" + loginUser.id, {
      method: "GET"
    })
      .then((response) => response.json())
      .then(data => {
        setResults(data);
      })
  }, []
  )
  //resultをanalyzeListに代入
  const analyzeList = results
  //日付のフォーマット指定
  const date = analyzeList.map(
    (result) =>
      result.createdDate
  );
  const n = moment(date[0])
  const p = moment(date[1])
  //最新の実施結果
  const newDate = n.format('YYYY年MM月DD日')
  //前回の実施結果
  const pastDate = p.format('YYYY年MM月DD日')
  // 表示するデータを配列として定義
  const resultData = analyzeList.map(
    (result) =>
    ([
      { type: 'ドライブ', A: result.drive_type, fullMark: 20 },
      { type: 'ボランティア', A: result.volunteer_type, fullMark: 20 },
      { type: 'アナライズ', A: result.analyze_type, fullMark: 20 },
      { type: 'クリエイト', A: result.create_type, fullMark: 20 }
    ])
  );

  //最新の結果のフラグの結果を抽出
  const createtypes = analyzeList.map((flg) =>
    flg.create_flg);


  const volunteertypes = analyzeList.map((flg) =>
    flg.volunteer_flg);


  const drivetypes = analyzeList.map((flg) =>
    flg.drive_flg);


  const analyzetypes = analyzeList.map((flg) =>
    flg.analyze_flg);
  return (
    <div className="radarChart">
      <RadarChart  // レーダーチャート全体の設定を記述
        cx="50%"  // 要素の左端とチャートの中心点との距離(0にするとチャートの左半分が隠れる)
        cy="50%"  // 要素の上部とチャートの中心点との距離(0にするとチャートの上半分が隠れる)
        outerRadius={200}  // レーダーチャート全体の大きさ
        width={700}  // レーダーチャートが記載される幅(この幅よりチャートが大きい場合、はみ出た箇所は表示されない)
        height={600}   // レーダーチャートが記載される高さ
        data={resultData[0]} // 表示対象のデータ
        className="radarChart"
      >
        {/* レーダーチャートの蜘蛛の巣のような線 */}
        <PolarGrid />
        {/* 項目を決めるデータのキー(サンプルでいう数学や歴史) */}
        <PolarAngleAxis dataKey="type" />

        {/* 目安となる数値が表示される線を指定  */}
        <PolarRadiusAxis
          angle={60}  // 中心点から水平を0°とした時の角度 垂直にしたいなら90を指定
          domain={[0, 20]}  // リストの１番目の要素が最小値、2番目の要素が最大値
        />

        {/* レーダーを表示 */}
        <Radar
          name={newDate}  // そのチャートが誰のデータか指定(チャート下にここで指定した値が表示される)
          dataKey="A"   // 表示する値と対応するdata内のキー
          stroke="#FF9503"  // レーダーの外枠の色
          fill="#FFC612"  // レーダー内の色
          fillOpacity={0.8}  // レーダー内の色の濃さ(1にすると濃さMAX)
        />
        {/* ２個目のレーダー */}
        {analyzeList[1] && <Radar name={pastDate} dataKey="A" stroke="#82ca9d" fill="#82ca9d" fillOpacity={0.6} data={resultData[1]} />}

        {/* 3個目のレーダー(時間があったらやる) */}
        {/*<Radar name="前々回" dataKey="A" stroke="#0066FF" fill="#0066FF" fillOpacity={0.9} data={resultData[2]}/>*/}


        {/* グラフの下の表記(<Radar>で指定したnameが表示される) */}
        <Legend />
      </RadarChart>
      {/* タイプ結果表示 */}
      <a>{loginUser.name}さんは</a>
      {createtypes[0] && <a className="typeFont">クリエイト</a>}
      {volunteertypes[0] && <a className="typeFont">ボランティア</a>}
      {drivetypes[0] && <a className="typeFont">ドライブ</a>}
      {analyzetypes[0] && <a className="typeFont">アナライズ</a>}
      <a>の傾向が強いです</a>
    </div>
  );//return終わり
}//class終わり

export default withCookies(TopRadarChart);
