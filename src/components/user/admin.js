import React, {Component} from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import * as actions from '../../action/actions.js';
import { withCookies } from 'react-cookie';
import '../../css/admin.css';
import {Link} from 'react-router-dom'

class Admin extends Component {
  constructor(props) {
    super(props)
    const {dispatch} = props;
    const { cookies } = this.props;
    this.state = {
      users: [],
      departmentId: [],
      //ログインユーザー情報の取得
      loginUser: cookies.get('user')
    }
    this.action = bindActionCreators(actions, dispatch);
  }

  componentDidMount() {
    fetch("http://localhost:8080/osushi/sendDepartment", {
      method: "GET"
    })
    .then((response) =>{
      response.json().
      then(json => { // オブジェクトに変換したレスポンスを受け取り、
        //this.action.departmentId(json) // Stateを更新する
        this.setState({departmentId: json})
      })
    })
    fetch("http://localhost:8080/osushi/getUsers", { // データを取得しに行く
      method: "GET"
    })
    .then((response) => { // json型のレスポンスをオブジェクトに変換する
      response.json().
      then(json => { // オブジェクトに変換したレスポンスを受け取り、
        this.action.getUsers(json) // Stateを更新する
        this.setState({users: json})
      })
    })
  }



  render() {
    //管理者バリデーション
    if (!this.state.loginUser.adminFlg) {
      alert("権限がありません\nトップに戻ります")
      this.props.history.push("/")
    }
    const userList = this.state.users
    const departmentList = this.state.departmentId

    const userIndex = userList.map( (user) => (
      <tr key={ user.id }>
          <td>{ user.id }</td>
          <td>{ user.account }</td>
          <td>{ user.name }</td>
          <td>{departmentList.map( (department) => (
            <div key={ department.id }>
              { user.departmentId === department.id && department.name }
            </div>
          ))}</td>
          <td><Link to={{pathname: '/result', state: {user: user}}}className="adminButton">閲覧</Link></td>
          <td><Link to={{pathname: '/edit', state: {user: user}}} className="adminButton">編集</Link></td>
      </tr>
    ));

    return(
      <div>
        <table>
          <tr><th>ID</th><th>アカウント</th><th>名前</th><th>部署</th><th>診断結果</th><th></th></tr>
          {userIndex}

        </table>
      </div>
    )
  }
}
Admin.propTypes = {
  dispatch: PropTypes.func,
  departmentId: PropTypes.any,
  users: PropTypes.any,
  history: PropTypes.object,
  cookies: PropTypes.any
}

function mapStateToProps(state) {
  return state
}
export const AdminPage =  withCookies(withRouter(connect(mapStateToProps)(Admin)))
