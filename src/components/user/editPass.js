import React, { useState } from "react"
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import 'whatwg-fetch';
import { withCookies,useCookies } from 'react-cookie';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../css/editPass.css';
import {Button} from 'react-bootstrap';

function EditPass(props){

  //クッキーからログイン情報取得
  const [cookies] = useCookies(['user']);
  const loginUser = cookies.user

  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")

  //バリデーション
  const [passwordError, setPasswordError] = useState("")
  const [confirmPasswordError, setConfirmPasswordError] = useState("")

  //パスワード入力時の処理
  const inputPassword = (e) => {
     setPassword(e.target.value)
  }

  //確認用パスワード入力時の処理
  const inputConfirmPassword = (e) => {
    setConfirmPassword(e.target.value)
  }

//送信前のバリデーションチェック
const handleClick = () => {
    //確認


    let count = 0;
    //初期化
    setPasswordError("")
    setConfirmPasswordError("")

    if (password=="" || password.length > 20  ) {
      setPasswordError("パスワードは1文字以上20文字以下で入力してください")
      count++
    }else if (!password.match(/\S/g)){
      setPasswordError("スペースは入力できません")
      count++
    }

    if (confirmPassword !== password) {
      setConfirmPasswordError("確認用パスワードが一致しません")
      count++
    }

    if (count === 0) {
       postSend()
    } else {
      return
    }
  }

    //更新ボタン押したときの処理
    //ユーザーのIDと変更後のパスワードを送信
    const postSend = () => {

      var confirmation = confirm('パスワードを変更しますか');
        if(!confirmation) {
          return
      }

      const data = {
        //ユーザーIDとパスワードを送信
        id:loginUser.id,
        password:password
      }
      fetch("http://localhost:8080/osushi/edit_pass", {
          method: "POST",
          body: JSON.stringify(data)
      })
      .then((response) => {
        if (response.status === 200) {
          props.history.push("/")
        } else {
          alert("更新に失敗しました。")
        }
      })
      .catch(error => console.error(error))
    }


    return (
      <form className="needs-validation">
        <div className="input">
          <div className="title">パスワード変更</div>
          <div className="form-floating mb-3">
            <input type="password" size="30" name="password" className="form-control" placeholder="パスワード" id="inputPassworrd" onChange={inputPassword} />
            <label htmlFor="floatingAccount">パスワード</label>
            <p className="editPass-error">{passwordError}</p>
          </div>

          <div className="form-floating mb-3">
            <input type="password" size="30" name="confirmpassword" className="form-control" placeholder="パスワード(確認)" id="inputConfirmPassworrd" onChange={inputConfirmPassword} />
            <label htmlFor="floatingAccount">パスワード(確認)</label>
            <p className="editPass-error">{confirmPasswordError}</p>
          </div>
          <Button className="editPass-button" variant="outline-primary" type="button" onClick={handleClick}>更新</Button>
      </div>
    </form>
    )//return閉じ
}//class閉じ

EditPass.propTypes = {
  history: PropTypes.object,
}

export default withCookies(withRouter(EditPass))
