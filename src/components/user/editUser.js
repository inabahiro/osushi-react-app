import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { withCookies } from 'react-cookie';
import 'whatwg-fetch';
import * as actions from '../../action/actions'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button } from 'react-bootstrap';

class EditUser extends Component {

  constructor(props) {
    super(props)
    const { dispatch } = props;
    const { cookies } = this.props;
    this.action = bindActionCreators(actions, dispatch)
    this.state = {
      loginUser: cookies.get('user'),
      name: "",
      password: "",
      passwordConfirm: "",
      errorMessageName: "",
      errorMessagePassword: "",
      errorMessagePasswordConfirm: "",
      isSendName: false,
      isSendPass: false,
      isSendPassConfirm: false
    }
  }

  render() {
    if (!this.state.loginUser.adminFlg) {
      alert("管理権限を持つアカウントでアクセスしてください\nトップに戻ります")
      window.location.href = "../"
    }

    const handleChaged = (e) => {
      this.setState({ [e.target.name]: e.target.value })
    }

    const handleChagedName = (e) => {
      if (validName(e.target.value)) {
        this.setState({ name: e.target.value })
      }
    }

    const handleChagedPassword = (e) => {
      console.warn(e.target.value)
      if (validPass(e.target.value)) {
        this.setState({ password: e.target.value })
      }
    }

    const handleChagedPasswordConfirm = (e) => {
      if (validPassConf(e.target.value)) {
        this.setState({ passwordConfirm: e.target.value })
      }
    }

    const sendButtonClicked = () => {
      //登録ボタン押下時の確認アラート
      if (this.state.name.length === 0 && this.state.password.length === 0) {
        alert("変更はありません")
        return
      }
      confirm("変更してよろしいですか？") && postSend()
      return window.location.href = "../admin"
    }

    const validName = (name) => {
      if (name.length > 20) {
        this.setState({ errorMessageName: "氏名は20文字以内で入力してください" })
        return false;
      } else if (name.length <= 20) {
        this.setState({ errorMessageName: "" })
        return true
      }
    }

    const validPass = (password) => {
      if (password.length <= 20 && password.match(/^[0-9a-zA-Z]+$/)) {
        this.setState({ errorMessagePassword: "      " })
        return true
      } else if (password.length === 0 && this.state.passwordConfirm.length ===0)  {
        this.setState({ errorMessagePassword: "" })
        this.setState({ errorMessagePasswordConfirm: "" })
        return true
      } else if (password.length > 20) {
        this.setState({ errorMessagePassword: "パスワードは20文字以内で入力してください" })
        return false;
      } else if (!password.match(/^[0-9a-zA-Z]+$/)) {
        this.setState({ errorMessagePassword: 'パスワードには半角英数字(スペース除く)のみ使用可能です' })
        return false;
      }
    }
   
    const validPassConf = (passwordConfirm) => {
      if (passwordConfirm === this.state.password) {
        this.setState({ errorMessagePassword: "" })
        this.setState({ errorMessagePasswordConfirm: "" })
      }
      else if (passwordConfirm === 0) {
        this.setState({ errorMessagePasswordConfirm: "" })
        return true
      }
      else if (this.state.password.length === 0) {
        this.setState({ errorMessagePasswordConfirm: "パスワードが未入力です" })
        return false
      } else if (this.state.password !== passwordConfirm) {
        this.setState({ errorMessagePasswordConfirm: "パスワードが一致しません" })
        return false
      }
    }

    const postSend = () => {
      // stateの値をJSONに整形
      const data = {
        id: this.props.location.state.user.id,
        account: this.state.account,
        adminFlg: this.state.adminFlg,
        name: this.state.name,
        password: this.state.password,
        departmentId: this.state.departmentId
      }
      // POST送信実⾏
      fetch("http://localhost:8080/osushi/edit", {
        method: "POST",
        body: JSON.stringify(data) //これがUserController.javaに渡されてる
      })
        // レスポンス返却時の処理
        .then((response) => {
          if (response.status !== 200) {
            alert("送信に失敗しました。ステータス")
          }
        })
        .catch(error => console.error(error));
    }
    let buttun
    if (this.state.errorMessageName == "" && this.state.errorMessagePassword == "" && this.state.errorMessagePasswordConfirm == "") {
      buttun = <Button variant="outline-primary" className="loginbutton" onClick={sendButtonClicked}>変更する</Button>

    } else {
      buttun = <Button variant="outline-primary" className="loginbutton" disabled onClick={sendButtonClicked}>変更する</Button>
    }
    if (this.props.location.state == undefined) {
      return (
        <h3>🙅管理画面からユーザーを選択してください</h3>
      )
    } else {
      return (
        <div className="needs-validation">
          <p className="signupTitle">ユーザー編集</p>
          <form noValidate>
            <label htmlFor="account">アカウント</label><br />
            <input className="signupIn" type="text" name="account" value={this.props.location.state.user.account} onChange={handleChaged}></input><br />
            <label htmlFor="name">氏名</label>
            <p><input className="signupIn" type="text" name="name" placeholder={this.props.location.state.user.name} onChange={handleChagedName} ></input></p>
            {this.state.errorMessageName && <p className="loginValidation">{this.state.errorMessageName}</p>}


            {/*セレクトボタン*/}
            <label htmlFor="adminFlg">権限</label>
            <p><select className="signupIn" name="adminFlg" id="adminFlg" onChange={handleChaged}>
              {/*booleam型でDBに登録するため、valueはtrue/falseで設定する*/}
              <option value=""></option>
              <option value="true">管理者</option>
              <option value="false">一般</option>
            </select></p>

            {/*セレクトボタン*/}
            <label htmlFor="adminFlg">所属</label>
            <p><select className="signupIn" name="departmentId" id="departmentId" onChange={handleChaged}>
              {/*valueの数字がdepartmentテーブルのidの内容になるように設定する*/}
              <option value=""></option>
              <option value="1">情報管理部</option>
              <option value="2">総務部</option>
              <option value="3">人事部</option>
              <option value="4">経理部</option>
              <option value="5">営業部</option>
            </select></p>

            <label htmlFor="password">パスワード</label>
            <p><input className="signupIn" type="password" name="password" onChange={handleChagedPassword}></input></p>
            {this.state.errorMessagePassword && <p className="loginValidation">{this.state.errorMessagePassword}</p>}

            <label htmlFor="password">パスワード(確認用)</label>
            <p><input className="signupIn" type="password" name="passwordConfim" onChange={handleChagedPasswordConfirm}></input></p>
            {this.state.errorMessagePasswordConfirm && <p className="loginValidation">{this.state.errorMessagePasswordConfirm}</p>}
            {buttun}
          </form>
        </div>
      )
    }
  }
}

EditUser.propTypes = {
  dispatch: PropTypes.func,
  history: PropTypes.object,
  location: PropTypes.any,
  cookies: PropTypes.any,
}

function mapStateToProps(state) {
  return state
}

export default withCookies(withRouter(connect(mapStateToProps)(EditUser)))
