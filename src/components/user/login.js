import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import 'whatwg-fetch';
import * as actions from '../../action/actions.js';
import { withRouter } from "react-router";
import { withCookies } from 'react-cookie';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button } from 'react-bootstrap';
import '../../css/login.css';


class Login extends Component {

  constructor(props) {
    super(props)
    const { dispatch } = props;
    this.action = bindActionCreators(actions, dispatch);
    this.state = {
      account: "",
      password: "",
      loginUser: [],
      accountError:'',
      passwordError:'',
      loginError:'',
      json: [],
    }
  }


  render(){

    const handleChaged = (e) => {
      this.setState({ [e.target.name]: e.target.value })
    }


    const loginSubmit = () => {

      //バリデーション
      this.setState({loginError: ''})
      this.setState({accountError:""})
      this.setState({passwordError:""})
      let count = 0
      if (this.state.account=="") {
        this.setState({accountError:"アカウントが未入力です"})
        count++
      }
      if (this.state.password=="") {
        this.setState({passwordError:"パスワードが未入力です"})
        count++
      }
      if(count>0){
        return
      }

      const { cookies } = this.props;
      const data = { account: this.state.account, password: this.state.password };

      this.setState({loginError:""})

      fetch("http://localhost:8080/osushi/login", {
        headers: {'Content-Type': 'application/json'},
        method: "POST",
        body: JSON.stringify(data)
      })
      .then((response) => {
        if (response.status === 200) {
          response.json()
          .then(json => {
            this.action.login(json);
            this.action.loginState(true);
            cookies.set('user', json, { path: "/" });
            cookies.set('isSignedIn', true, { path: "/" });
            window.location.href = "../"
          })
        }else{
          this.setState({loginError: 'アカウントまたはパスワードが誤っています'})
          return
        }
    })
  }

    return(
        <div className="needs-validation">
          <div className="title">LoginPage</div>
          <div className="form-floating mb-3">
            <label htmlFor="inputUserName" className="col-sm-3 col-form-label"></label>
            <div>
              {this.state.loginError}
              <input type="text" name="account" className="form-control" id="inputAccount" placeholder="Account" onChange={handleChaged} required></input>
              {this.state.accountError}
            </div>
          </div>
          <div className="form-floating mb-3">
            <label htmlFor="floatingPassword"></label>
            <div>
              <input type="password" name="password" className="form-control" id="inputPassword" placeholder="Password" onChange={handleChaged} required></input>
              {this.state.passwordError}
            </div>
          </div>
          <Button className="loginbutton"type="submit" variant="outline-primary" onClick={loginSubmit} >ログイン</Button>
        </div>
    )//return閉じ
  }//render閉じ
}


Login.propTypes = {
  dispatch: PropTypes.func,
  cookies: PropTypes.any,
  user: PropTypes.any,
  isSignedIn: PropTypes.bool,
  loginUser: PropTypes.any,
  isMissLogin: PropTypes.any,
}

function mapStateToProps(state) {
  return state;
}

export default withCookies(withRouter(connect(mapStateToProps)(Login)));
