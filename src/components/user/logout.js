import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../action/actions'
import PropTypes from 'prop-types';
import { withCookies } from 'react-cookie';
import { Link } from 'react-router-dom';


class Logout extends Component {
    constructor(props) {
        super(props);
        const{dispatch} = props;
        this.action = bindActionCreators(actions, dispatch);
    }

    // ログアウト処理
    handleClick() {
        //ログイン情報変更
        this.action.login([]);
        this.action.loginState(false);
        // クッキー情報変更
        const { cookies } = this.props;
        /*setCookie(name, value, [options])で指定のクッキーの値を設定。
        optionsにはRFC6265に準じたクッキーオプション（ドメイン、パス、有効期限、SameSiteなど）を指定。
        */
        cookies.set('isSignedIn','');
        cookies.set('user', '[]');
        this.props.history.push("/login")
    }

    render() {
        return(
            <li><Link to="/logout" className="link" onClick={() => this.handleClick()}>ログアウト</Link></li>
        )
    }
}

Logout.propTypes = {
    dispatch: PropTypes.func,
    user: PropTypes.any,
    isSignedIn: PropTypes.bool,
    cookies: PropTypes.any,
    history: PropTypes.object,
}

function mapStateToProps(state) {
    return state;
}

export default withCookies(connect(mapStateToProps)(Logout));
