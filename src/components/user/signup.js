import React, {Component} from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { withCookies } from 'react-cookie';
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import 'whatwg-fetch';
import * as actions from '../../action/actions.js';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button } from 'react-bootstrap';

class Signup extends Component {

  constructor(props) {
    super(props)
    const {dispatch} = props;
    const { cookies } = this.props;
    this.action = bindActionCreators(actions, dispatch);
    this.state = {
      //登録内容
      account:'',
      adminFlg:'',
      name:'',
      password:'',
      passwordConfim:'',
      departmentId:'',

      //バリデーションメッセージ
      accountError:'',
      adminFlgError:'',
      nameError:'',
      passwordFlgError:'',
      passwordConfimError:'',
      departmentIdError:'',

      //ログインユーザー情報の取得
      loginUser: cookies.get('user'),

      //ユーザー全権取得の為の配列
      users:[]
    }
    this.action = bindActionCreators(actions, dispatch);
  }
  //全ユーザー情報の取得
  componentDidMount() {
    fetch("http://localhost:8080/osushi/getUsers", {
      method: "GET"
    })
    .then((response) => {
      response.json().
      then(json => {
        this.setState({users: json})
      })
    })
  }

  render() {
    //管理者バリデーション
    if (!this.state.loginUser.adminFlg) {
      alert("権限がありません\nトップに戻ります")
      this.props.history.push("/")
    }
    //イベント
    const handleChaged = (e) => {
      this.setState({[e.target.name]: e.target.value})
    }

    //登録ボタン押下時の処理
    const sendButtonClicked = () => {

      //カウントの初期化
      let count = 0;

      //取得したユーザー情報からアカウントのみ抽出
      const userList = this.state.users
      const usersAccount = userList.map((user) =>
      user.account);

      //アカウントバリデーション
      for(let i = 0;  i < usersAccount.length; i++){
        if(this.state.account == ''){
          this.setState({accountError:'アカウントは必須です'})
          count++
        }else if(!this.state.account.match(/^[0-9]{8}$/)){
          this.setState({accountError:'アカウントは半角数字8桁で入力して下さい'})
          count++
        }else if(this.state.account == usersAccount[i]){
          this.setState({accountError:'既に存在するアカウントです'})
          count++
          break;//重複した時点でfor文処理を終えるためにbreak;
        }else{
          this.setState({accountError:''})
        }
      }

      //氏名バリデーション
      if(this.state.name == ''){
        this.setState({nameError:'氏名は必須です'})
        count++
      }else if(!this.state.name.match(/^[ぁ-んァ-ヶ一-龠々a-zA-Z]+$/)){
        this.setState({nameError:'氏名に数字・記号・スペース(半角/全角)は使用できません'})
        count++
      }else if(this.state.name.length > 20){
        this.setState({nameError:'氏名は20文字以内で入力して下さい'})
        count++
      }else{
        this.setState({nameError:''})
      }

      //権限バリデーション
      if(this.state.adminFlg == ''){
        this.setState({adminFlgError:'権限は必須です'})
        count++
      }else{
        this.setState({adminFlgError:''})
      }

      //所属バリデーション
      if(this.state.departmentId == ''){
        this.setState({departmentIdError:'所属は必須です'})
        count++
      }else{
        this.setState({departmentIdError:''})
      }

      //パスワードバリデーション
      if(this.state.password == ''){
        this.setState({passwordError:'パスワードは必須です'})
        count++
      }else if(!this.state.password.match(/^[0-9a-zA-Z]+$/)){
        this.setState({passwordError:'パスワードには半角英数字以外使用できません'})
        count++
      }else if(this.state.password.length > 20){
        this.setState({passwordError:'パスワードは20文字以内で入力してください'})
        count++
      }else{
        this.setState({passwordError:''})
      }

      //パスワード(確認用)バリデーション
      if(this.state.passwordConfim == ''){
        this.setState({passwordConfimError:'パスワード(確認用)は必須です'})
        count++
      }else if(this.state.passwordConfim != this.state.password){
        this.setState({passwordConfimError:'パスワードが一致しません'})
        count++
      }else{
        this.setState({passwordConfimError:''})
      }

      //正常時の確認ダイアログ
      if(count === 0){
        confirm("この内容で登録しますか？") &&
        postSend() &&
        confirm("登録しました")
      }else{
        return
      }
    }

    //入力した登録情報をJOSONにデータに整形し、サーバー側のAPIにPOST送信する関数
    const postSend = () => {
      // stateの値をJSONに整形
      const data = {
        account: this.state.account,
        adminFlg: this.state.adminFlg,
        name: this.state.name,
        password: this.state.password,
        departmentId: this.state.departmentId
      }
      // POST送信実⾏
      fetch("http://localhost:8080/osushi/addUser", {
        method: "POST",
        body: JSON.stringify(data) //これがUserController.javaに渡されてる
      })
      // レスポンス返却時の処理
      .then((response) => {
        if(response.status === 200) {
          alert("登録しました")
          this.props.history.push("/")
        } else {
          alert("登録に失敗しました")
        }
      })
      .catch(error => console.error(error));
    }
    return(
      <div className="needs-validation">
      <p className="signupTitle">ユーザー登録</p>
      <span style={{ color: "red" }}>{this.state.message}</span>
      <label htmlFor="account">アカウント</label>
      <p><input className="signupIn" type="text" name="account" onChange={handleChaged}></input></p>
      <p className="loginValidation">{this.state.accountError}</p>

      <label htmlFor="name">氏名</label>
      <p><input className="signupIn" type="text" name="name" onChange={handleChaged}></input></p>
      <p className="loginValidation">{this.state.nameError}</p>

      {/*セレクトボタン*/}
      <label htmlFor="adminFlg">権限</label>
      <p><select className="signupIn" name="adminFlg" id="adminFlg" value={this.state.adminFlg} onChange={handleChaged}>
      {/*booleam型でDBに登録するため、valueはtrue/falseで設定する*/}
      <option value=""></option>
      <option value="true">管理者</option>
      <option value="false">一般</option>
      </select></p>
      <p className="loginValidation">{this.state.adminFlgError}</p>

      {/*セレクトボタン*/}
      <label htmlFor="adminFlg">所属</label>
      <p><select className="signupIn" name="departmentId" id="departmentId" value={this.state.departmentId} onChange={handleChaged}>
      {/*valueの数字がdepartmentテーブルのidの内容になるように設定する*/}
      <option value=""></option>
      <option value="1">情報管理部</option>
      <option value="2">総務部</option>
      <option value="3">人事部</option>
      <option value="4">経理部</option>
      <option value="5">営業部</option>
      </select></p>
      <p className="loginValidation">{this.state.departmentIdError}</p>

      <label htmlFor="password">パスワード</label>
      <p><input className="signupIn" type="password" name="password" onChange={handleChaged}></input></p>
      <p className="loginValidation">{this.state.passwordError}</p>

      <label htmlFor="password">パスワード(確認用)</label>
      <p><input className="signupIn" type="password" name="passwordConfim" onChange={handleChaged}></input></p>
      <p className="loginValidation">{this.state.passwordConfimError}</p>

      <Button variant="outline-primary" className="loginbutton" onClick={sendButtonClicked}>登録する</Button>
      </div>
    )//return終
  }//render終
}//class終

Signup.propTypes = {
  dispatch: PropTypes.func,
  history: PropTypes.object,
  users: PropTypes.any,
  cookies: PropTypes.any
}

function mapStateToProps(state) {
  return state
}

export default withCookies(withRouter(connect(mapStateToProps)(Signup)))
