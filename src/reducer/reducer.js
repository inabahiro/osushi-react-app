import * as actions from "../action/actions"



const defaultLoginUserState = {
  loginUser: []
}

const defaultLoginState = {
  isLogined: false
}

const defaultDepartmentState = {
  departmentId: []
}

const defaultUsersState = {
  users: []
}


export const loginUserReducer = (state = defaultLoginUserState.loginUser, action) => {

  switch (action.type) {
    case (actions.LOGIN):
      return {
        ...state,
        loginUser: action.loginUser
        //action.jsのtweetを参照
      }
      default:
        return state;
  }
}

export const departmentReducer = (state = defaultDepartmentState.departmentId, action) => {

  switch (action.type) {
    case (actions.DEPARTMENT):
      return {
        ...state,
        departmentId: action.departmentId
      }
      default:
        return state;
  }
}

export const usersReducer = (state = defaultUsersState.users, action) => {

  switch (action.type) {
    case (actions.USERS):
      return {
        ...state,
        users: action.users
      }
      default:
        return state;
  }
}


export const loginReducer = (state = defaultLoginState.isLogined, action) => {
  switch (action.type) {
    case (actions.LOGIN_STATE):
      return {
        ...state,
        isLogined: action.isLogined
      }
   default:
   return state;
  }
}
