import { createStore as reduxCreateStore, combineReducers } from 'redux'
import {loginUserReducer, loginReducer, departmentReducer, usersReducer } from '../reducer/reducer';

export default function createStore(){
  return reduxCreateStore(
    combineReducers({
      loginUser: loginUserReducer,
      isLogined: loginReducer,
      departmentId: departmentReducer,
      users: usersReducer,
    })
  )
}
